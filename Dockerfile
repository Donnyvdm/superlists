FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY requirements.txt dev_requirements.txt /code/
RUN pip install -r requirements.txt && pip install -r dev_requirements.txt
COPY . /code/
ENV VIRTUAL_ENV=/code/venv PATH="/code/venv/bin:$PATH"