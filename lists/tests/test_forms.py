import pytest

from lists.forms import (
    DUPLICATE_ITEM_ERROR,
    EMPTY_ITEM_ERROR,
    ExistingListItemForm,
    ItemForm,
    NewListForm,
)
from lists.models import List, Item


# New list tests
def test_form_item_input_has_placeholder_and_css_classes():
    form = ItemForm()

    assert 'placeholder="Enter a to-do item"' in form.as_p()
    assert 'class="form-control input-lg"' in form.as_p()


def test_new_form_validation_for_blank_items():
    form = ItemForm(data={"text": ""})
    assert not form.is_valid()
    assert form.errors["text"] == [EMPTY_ITEM_ERROR]


# NewListForm tests
def test_save_creates_new_list_from_post_data_if_user_not_authenticated(mocker):
    mock_List_create_new = mocker.patch("lists.forms.List.create_new")
    user = mocker.Mock(is_authenticated=False)
    form = NewListForm(data={"text": "new item text"})
    form.is_valid()
    form.save(owner=user)
    mock_List_create_new.assert_called_once_with(first_item_text="new item text")


def test_save_creates_new_list_with_owner_if_user_authenticated(mocker):
    mock_List_create_new = mocker.patch("lists.forms.List.create_new")
    user = mocker.Mock(is_authenticated=True)
    form = NewListForm(data={"text": "new item text"})
    form.is_valid()
    form.save(owner=user)
    mock_List_create_new.assert_called_once_with(
        first_item_text="new item text", owner=user
    )


def test_save_returns_new_list_object(mocker):
    mock_List_create_new = mocker.patch("lists.forms.List.create_new")
    user = mocker.Mock(is_authenticated=True)
    form = NewListForm(data={"text": "new item text"})
    form.is_valid()
    response = form.save(owner=user)
    assert response == mock_List_create_new.return_value


# Existing list tests
@pytest.mark.django_db
def test_form_renders_item_text_input():
    list_ = List.objects.create()
    form = ExistingListItemForm(for_list=list_)
    assert 'placeholder="Enter a to-do item"' in form.as_p()


@pytest.mark.django_db
def test_form_validation_for_blank_items():
    list_ = List.objects.create()
    form = ExistingListItemForm(for_list=list_, data={"text": ""})
    assert not form.is_valid()
    assert form.errors["text"] == [EMPTY_ITEM_ERROR]


@pytest.mark.django_db
def test_existing_form_validation_for_duplicate_items():
    list_ = List.objects.create()
    Item.objects.create(list=list_, text="no twins!")
    form = ExistingListItemForm(for_list=list_, data={"text": "no twins!"})
    assert not form.is_valid()
    assert form.errors["text"] == [DUPLICATE_ITEM_ERROR]


@pytest.mark.django_db
def test_form_save():
    list_ = List.objects.create()
    form = ExistingListItemForm(for_list=list_, data={"text": "hi"})
    new_item = form.save()
    assert new_item == Item.objects.all()[0]
