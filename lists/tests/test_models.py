from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
import pytest

from lists.models import Item, List

User = get_user_model()

# Item model
@pytest.mark.django_db
def test_default_text():
    item = Item()
    assert item.text == ""


@pytest.mark.django_db
def test_item_is_related_to_list():
    list_ = List.objects.create()
    item = Item()
    item.list = list_
    item.save()
    assert item in list_.item_set.all()


@pytest.mark.django_db
def test_cannot_save_empty_list_items():
    list_ = List.objects.create()
    item = Item(list=list_, text="")
    with pytest.raises(ValidationError):
        item.save()
        item.full_clean()


@pytest.mark.django_db
def test_duplicate_items_are_invalid():
    list_ = List.objects.create()
    Item.objects.create(list=list_, text="bla")
    with pytest.raises(ValidationError):
        item = Item(list=list_, text="bla")
        item.full_clean()


@pytest.mark.django_db
def test_CAN_save_same_item_to_different_lists():
    list1 = List.objects.create()
    list2 = List.objects.create()
    Item.objects.create(list=list1, text="bla")
    item = Item(list=list2, text="bla")
    item.full_clean()  # should not raise


@pytest.mark.django_db
def test_string_representation():
    item = Item(text="some text")
    assert str(item) == "some text"


@pytest.mark.django_db
def test_list_ordering():
    list1 = List.objects.create()
    item1 = Item.objects.create(list=list1, text="i1")
    item2 = Item.objects.create(list=list1, text="item 2")
    item3 = Item.objects.create(list=list1, text="3")
    assert list(Item.objects.all()) == [item1, item2, item3]


# List model
@pytest.mark.django_db
def test_get_absolute_url():
    list_ = List.objects.create()
    assert list_.get_absolute_url() == f"/lists/{list_.id}/"


@pytest.mark.django_db
def test_lists_can_have_owners():
    List(owner=User())  # should not raise


@pytest.mark.django_db
def test_list_owner_is_optional():
    List().full_clean()  # should not raise


@pytest.mark.django_db
def test_create_new_creates_list_and_first_item():
    List.create_new(first_item_text="new item text")
    new_item = Item.objects.first()
    assert new_item.text == "new item text"
    new_list = List.objects.first()
    assert new_item.list == new_list


@pytest.mark.django_db
def test_create_new_optionally_saves_owner():
    user = User.objects.create()
    List.create_new(first_item_text="new item text", owner=user)
    new_list = List.objects.first()
    assert new_list.owner == user


@pytest.mark.django_db
def test_create_returns_new_list_object():
    returned = List.create_new(first_item_text="new item text")
    new_list = List.objects.first()
    assert returned == new_list


@pytest.mark.django_db
def test_list_name_is_first_item_text():
    list_ = List.objects.create()
    Item.objects.create(list=list_, text="first item")
    Item.objects.create(list=list_, text="second item")
    assert list_.name == "first item"
