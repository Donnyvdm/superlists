from django.contrib.auth import get_user_model
from django.http import HttpRequest
from django.utils.html import escape
import pytest

from lists.forms import (
    DUPLICATE_ITEM_ERROR,
    EMPTY_ITEM_ERROR,
    ExistingListItemForm,
    ItemForm,
)
from lists.models import Item, List
from lists.views import new_list
from lists.tests.utils import post_invalid_input, template_used

User = get_user_model()

# Homepage tests
@pytest.mark.django_db
def test_home_page_returns_correct_html(client):
    response = client.get("/")
    assert template_used(response, "home.html")


@pytest.mark.django_db
def test_home_page_uses_item_form(client):
    response = client.get("/")
    assert isinstance(response.context["form"], ItemForm)


# New list view
@pytest.mark.django_db
def test_can_save_a_POST_request(client):
    client.post("/lists/new", data={"text": "A new list item"})
    assert Item.objects.count() == 1
    new_item = Item.objects.first()
    assert new_item.text == "A new list item"


@pytest.mark.django_db
def test_for_invalid_input_doesnt_save_but_shows_errors(client):
    response = client.post("/lists/new", data={"text": ""})
    assert response.status_code == 200
    assert template_used(response, "home.html")
    assert escape(EMPTY_ITEM_ERROR) in response.content.decode()
    assert List.objects.count() == 0


@pytest.mark.django_db
def test_list_owner_is_saved_if_user_is_authenticated(client):
    user = User.objects.create(email="a@b.com")
    client.force_login(user)
    client.post("/lists/new", data={"text": "new item"})
    list_ = List.objects.first()
    assert list_.owner == user


# New List View
class TestNewListForm:
    @pytest.fixture(autouse=True)
    def mock_newlistform(self, mocker):
        self.mockNewListForm = mocker.patch("lists.views.NewListForm")
        self.mockNewListForm.return_value.save.return_value.get_absolute_url.return_value = (  # noqa
            "fakeurl"
        )

    @pytest.fixture(autouse=True)
    def new_list_post_request(self, mocker):
        request = HttpRequest()
        request.POST["text"] = "new list item"
        request.user = mocker.Mock()
        self.request = request

    def test_passes_POST_data_to_NewListForm(self):
        new_list(self.request)
        self.mockNewListForm.assert_called_once_with(data=self.request.POST)

    def test_saves_form_with_owner_if_form_valid(self):
        mock_form = self.mockNewListForm.return_value
        mock_form.is_valid.return_value = True
        new_list(self.request)
        mock_form.save.assert_called_once_with(owner=self.request.user)

    def test_redirects_to_form_returned_object_if_form_valid(self, mocker):
        mock_redirect = mocker.patch("lists.views.redirect")

        mock_form = self.mockNewListForm.return_value
        mock_form.is_valid.return_value = True

        response = new_list(self.request)

        assert response == mock_redirect.return_value
        mock_redirect.assert_called_once_with(mock_form.save.return_value)

    def test_renders_home_template_with_form_if_form_invalid(self, mocker):
        mock_render = mocker.patch("lists.views.render")
        mock_form = self.mockNewListForm.return_value
        mock_form.is_valid.return_value = False

        response = new_list(self.request)

        assert response == mock_render.return_value
        mock_render.assert_called_once_with(
            self.request, "home.html", {"form": mock_form}
        )

    def test_does_not_save_if_form_invalid(self):
        mock_form = self.mockNewListForm.return_value
        mock_form.is_valid.return_value = False
        new_list(self.request)
        assert not mock_form.save.called


# List view
@pytest.mark.django_db
def test_list_view_uses_list_template(client):
    list_ = List.objects.create()
    response = client.get(f"/lists/{list_.id}/")
    assert template_used(response, "list.html")


@pytest.mark.django_db
def test_displays_item_form(client):
    list_ = List.objects.create()
    response = client.get(f"/lists/{list_.id}/")
    assert isinstance(response.context["form"], ExistingListItemForm)
    assert 'name="text"' in response.content.decode()


@pytest.mark.django_db
def test_list_displays_only_items_for_that_list(client):
    correct_list = List.objects.create()
    Item.objects.create(text="itemey 1", list=correct_list)
    Item.objects.create(text="itemey 2", list=correct_list)

    other_list = List.objects.create()
    Item.objects.create(text="other list item 1", list=other_list)
    Item.objects.create(text="other list item 2", list=other_list)

    response = client.get(f"/lists/{correct_list.id}/")

    resp_text = response.content.decode()
    assert response.status_code == 200
    assert "itemey 1" in resp_text
    assert "itemey 2" in resp_text
    assert "other list item 1" not in resp_text
    assert "other list item 2" not in resp_text


@pytest.mark.django_db
def test_list_view_passes_correct_list_to_template(client):
    List.objects.create()
    correct_list = List.objects.create()
    response = client.get(f"/lists/{correct_list.id}/")
    assert response.context["list"] == correct_list


@pytest.mark.django_db
def test_can_save_a_POST_request_to_an_existing_list(client):
    List.objects.create()
    correct_list = List.objects.create()

    client.post(
        f"/lists/{correct_list.id}/", data={"text": "A new item for an existing list"}
    )

    assert Item.objects.count() == 1
    new_item = Item.objects.first()
    assert new_item.text == "A new item for an existing list"
    assert new_item.list == correct_list


@pytest.mark.django_db
def test_POST_redirects_to_list_view(client):
    List.objects.create()
    correct_list = List.objects.create()

    response = client.post(
        f"/lists/{correct_list.id}/", data={"text": "A new item for an existing list"}
    )

    assert response.status_code == 302
    assert response["location"] == f"/lists/{correct_list.id}/"


@pytest.mark.django_db
def test_for_invalid_input_nothing_saved_to_db(client):
    post_invalid_input(client)
    assert Item.objects.count() == 0


@pytest.mark.django_db
def test_for_invalid_input_renders_list_template(client):
    response = post_invalid_input(client)
    assert response.status_code == 200
    assert template_used(response, "list.html")


@pytest.mark.django_db
def test_list_view_invalid_input_passes_form_to_template(client):
    response = post_invalid_input(client)
    assert isinstance(response.context["form"], ExistingListItemForm)


@pytest.mark.django_db
def test_for_invalid_input_shows_error_on_page(client):
    response = post_invalid_input(client)
    assert escape(EMPTY_ITEM_ERROR) in response.content.decode()


@pytest.mark.django_db
def test_duplicate_item_validation_errors_end_up_on_lists_page(client):
    list1 = List.objects.create()
    Item.objects.create(list=list1, text="textey")
    response = client.post(f"/lists/{list1.id}/", data={"text": "textey"})

    expected_error = escape(DUPLICATE_ITEM_ERROR)
    assert expected_error in response.content.decode()
    assert template_used(response, "list.html")
    assert Item.objects.all().count() == 1


# My lists view
@pytest.mark.django_db
def test_my_lists_url_renders_my_lists_template(client):
    User.objects.create(email="a@b.com")
    response = client.get("/lists/users/a@b.com/")
    assert template_used(response, "my_lists.html")


@pytest.mark.django_db
def test_passes_correct_owner_to_template(client):
    User.objects.create(email="wrong@owner.com")
    correct_user = User.objects.create(email="a@b.com")
    response = client.get("/lists/users/a@b.com/")
    assert response.context["owner"] == correct_user

