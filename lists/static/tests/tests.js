var assert = chai.assert;

describe('Form', function () {

    before(function () {
        this.$fixture = $('#mocha-fixture').html();
    });

    beforeEach(function () {
        $('#mocha-fixture').html(this.$fixture);
    })
    describe("input box", function () {
        it('should hide errors on keypress', function () {
            window.Superlists.initialize();
            $('input[name="text"]').trigger('keypress');
            assert.equal($('.has-error').is(':visible'), false);
        });

        it('should show errors if there is no keypress', function () {
            window.Superlists.initialize();
            assert.equal($('.has-error').is(':visible'), true);
        })
    })

});