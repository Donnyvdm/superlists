import pytest

from accounts.models import Token
import accounts.views


# Send Email Login tests
@pytest.mark.django_db
def test_email_login_redirects_to_home_page(client):
    response = client.post(
        "/accounts/send_login_email", data={"email": "edith@example.com"}
    )
    assert response.status_code == 302
    assert response["location"] == f"/"


@pytest.mark.django_db
def test_sends_mail_to_address_from_post(client, mocker):
    mocker.patch("accounts.views.send_mail")

    client.post("/accounts/send_login_email", data={"email": "edith@example.com"})

    assert accounts.views.send_mail.called
    (subject, body, from_email, to_list), kwargs = accounts.views.send_mail.call_args
    assert subject == "Your login link for Superlists"
    assert from_email == "noreply@superlists"
    assert to_list == ["edith@example.com"]


@pytest.mark.django_db
def test_adds_success_message(client):
    response = client.post(
        "/accounts/send_login_email", data={"email": "edith@example.com"}, follow=True
    )

    message = list(response.context["messages"])[0]
    assert (
        message.message
        == "Check your email, we've sent you a link you can use to log in."
    )
    assert message.tags == "success"


class TestLoginView:
    @pytest.fixture(autouse=True)
    def mock_auth(self, mocker):
        mocker.patch("accounts.views.auth")

    @pytest.mark.django_db
    def test_login_view_redirects_to_home_page(self, client):
        response = client.get("/accounts/login?token=abcd123")
        assert response.status_code == 302
        assert response["location"] == f"/"

    @pytest.mark.django_db
    def test_calls_authenticate_with_uid_from_get_request(self, client):
        client.get("/accounts/login?token=abcd123")

        accounts.views.auth.authenticate.assert_called_with(uid="abcd123")

    @pytest.mark.django_db
    def test_calls_auth_login_with_user_if_there_is_one(self, client):
        response = client.get("/accounts/login?token=abcd123")

        accounts.views.auth.login.assert_called_with(
            response.wsgi_request, accounts.views.auth.authenticate.return_value
        )

    @pytest.mark.django_db
    def test_does_not_login_if_user_is_not_authenticated(self, client):
        accounts.views.auth.authenticate.return_value = None
        client.get("/accounts/login?token=abcd123")
        assert not accounts.views.auth.login.called


# Test Sending Emails
@pytest.mark.django_db
def test_creates_token_associated_with_email(client):
    client.post("/accounts/send_login_email", data={"email": "edith@example.com"})
    token = Token.objects.first()
    assert token.email, "edith@example.com"


@pytest.mark.django_db
def test_sends_link_to_login_using_token_uid(client, mocker):
    mocker.patch("accounts.views.send_mail")
    client.post("/accounts/send_login_email", data={"email": "edith@example.com"})

    token = Token.objects.first()
    expected_url = f"http://testserver/accounts/login?token={token.uid}"
    (subject, body, from_email, to_list), kwargs = accounts.views.send_mail.call_args
    assert expected_url in body

