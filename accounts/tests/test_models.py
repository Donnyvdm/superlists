from django.contrib import auth
import pytest

from accounts.models import Token

User = auth.get_user_model()


# User model tests
@pytest.mark.django_db
def test_user_is_valid_with_email_only():
    user = User(email="a@b.com")
    user.full_clean()  # should not raise


@pytest.mark.django_db
def test_email_is_primary_key():
    user = User(email="a@b.com")
    assert user.pk == "a@b.com"


@pytest.mark.django_db
def test_no_problem_with_auth_login(client):
    user = User.objects.create(email="edith@example.com")
    user.backend = ""
    request = client.request().wsgi_request
    auth.login(request, user)  # should not raise


# Token model tests
@pytest.mark.django_db
def test_links_user_with_auto_generated_uid():
    token1 = Token.objects.create(email="a@b.com")
    token2 = Token.objects.create(email="a@b.com")
    assert token1.uid != token2.uid

