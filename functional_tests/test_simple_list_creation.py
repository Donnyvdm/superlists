import re
import pytest

from functional_tests.utils import get_item_input_box, add_list_item


@pytest.mark.functional
@pytest.mark.django_db
def test_can_start_a_list_for_one_user(selenium, home_url):
    # Edith has heard about a cool new online to-do app. She goes
    # to check out its homepage
    browser = selenium
    browser.get(home_url)

    # She notices the page title and header mention to-do lists
    assert "To-Do" in browser.title
    header_text = browser.find_element_by_tag_name("h1").text
    assert "To-Do" in header_text

    # She is invited to enter a to-do item straight away
    inputbox = get_item_input_box(browser)
    assert inputbox.get_attribute("placeholder") == "Enter a to-do item"

    # She types "Buy peacock feathers" into a text box (Edith's hobby
    # is tying fly-fishing lures)
    add_list_item(browser, "Buy peacock feathers")

    # There is still a text box inviting her to add another item. She
    # enters "Use peacock feathers to make a fly" (Edith is very methodical)
    add_list_item(browser, "Use peacock feathers to make a fly")

    # Satisfied, she goes back to sleep


@pytest.mark.functional
@pytest.mark.django_db
def test_multiple_users_can_start_lists_at_different_urls(
    selenium_factory, live_server, home_url
):

    edith_browser = selenium_factory()

    # Edith starts a new to-do list
    edith_browser.get(home_url)

    add_list_item(edith_browser, "Buy peacock feathers")

    # She notices that her list has a unique URL
    edith_list_url = edith_browser.current_url
    assert re.search(r"/lists/.+", edith_list_url) is not None

    # Now a new user, Francis, comes along to the site.

    # We use a new browser session to make sure that no information
    # of Edith's is coming through from cookies etc
    francis_browser = selenium_factory()

    # Francis visits the home page.  There is no sign of Edith's
    # list
    francis_browser.get(home_url)
    page_text = francis_browser.find_element_by_tag_name("body").text
    assert "Buy peacock feathers" not in page_text
    assert "make a fly" not in page_text

    # Francis starts a new list by entering a new item. He
    # is less interesting than Edith...
    add_list_item(francis_browser, "Buy milk")

    # Francis gets his own unique URL
    francis_list_url = francis_browser.current_url
    assert re.search(r"/lists/.+", francis_list_url) is not None
    assert not francis_list_url == edith_list_url

    # Again, there is no trace of Edith's list
    page_text = francis_browser.find_element_by_tag_name("body").text
    assert "Buy peacock feathers" not in page_text
    assert "Buy milk" in page_text

    # Satisfied, they both go back to sleep
