import pytest
from selenium.webdriver.common.keys import Keys

from functional_tests.utils import (
    get_error_element,
    get_item_input_box,
    wait_for,
    wait_for_row_in_list_table,
    add_list_item,
)


@pytest.mark.functional
@pytest.mark.django_db
def test_cannot_add_empty_list_items(selenium, live_server, home_url):
    browser = selenium

    # Edith goes to the home page and accidentally tries to submit
    # an empty list item. She hits Enter on the empty input box
    browser.get(home_url)
    get_item_input_box(browser).send_keys(Keys.ENTER)

    # The browser intercepts the request, and does not load the
    # list page
    wait_for(lambda: browser.find_element_by_css_selector("#id_text:invalid"))

    # She starts typing some text for the new item and the error disappears
    get_item_input_box(browser).send_keys("Buy milk")
    wait_for(lambda: browser.find_element_by_css_selector("#id_text:valid"))

    # And she can submit it successfully
    get_item_input_box(browser).send_keys(Keys.ENTER)

    # Perversely, she now decides to submit a second blank list item
    get_item_input_box(browser).send_keys(Keys.ENTER)

    # Again, the browser will not comply
    wait_for_row_in_list_table(browser, "1: Buy milk")
    wait_for(lambda: browser.find_elements_by_css_selector("#id_text:invalid"))

    # And she can correct it by filling some text in
    get_item_input_box(browser).send_keys("Make tea")
    wait_for(lambda: browser.find_element_by_css_selector("#id_text:valid"))
    get_item_input_box(browser).send_keys(Keys.ENTER)
    wait_for_row_in_list_table(browser, "1: Buy milk")
    wait_for_row_in_list_table(browser, "2: Make tea")


@pytest.mark.functional
@pytest.mark.django_db
def test_cannot_add_duplicate_items(selenium, live_server, home_url):
    browser = selenium

    # Edith goes to the home page and starts a new list
    browser.get(home_url)
    add_list_item(browser, "Buy wellies")

    # She accidentally tries to enter a duplicate item
    get_item_input_box(browser).send_keys("Buy wellies")
    get_item_input_box(browser).send_keys(Keys.ENTER)

    # She sees a helpful error message
    wait_for(
        lambda: get_error_element(browser).text, "You've already got this in your list"
    )


@pytest.mark.functional
@pytest.mark.django_db
def test_error_messages_are_cleared_on_input(selenium, live_server, home_url):
    browser = selenium

    # Edith starts a list and causes a validation error:
    browser.get(home_url)
    add_list_item(browser, "Banter too thick")
    get_item_input_box(browser).send_keys("Banter too thick")
    get_item_input_box(browser).send_keys(Keys.ENTER)

    wait_for(lambda: get_error_element(browser).is_displayed())

    # She starts typing in the input box to clear the error
    get_item_input_box(browser).send_keys("a")

    # She is pleased to see that the error message disappears
    wait_for(lambda: not get_error_element(browser).is_displayed(), True)
