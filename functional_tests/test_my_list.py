import pytest

from functional_tests.utils import wait_for, add_list_item


@pytest.mark.functional
def test_logged_in_users_lists_are_saved_as_my_lists(authenticated_selenium, home_url):

    # Edith is a logged-in user
    browser = authenticated_selenium("edith@example.com")

    # She goes to the home page and starts a list
    browser.get(home_url)
    add_list_item(browser, "Reticulate splines")
    add_list_item(browser, "Immanentize eschaton")
    first_list_url = browser.current_url

    # She notices a "My lists" link, for the first time.
    browser.find_element_by_link_text("My lists").click()

    # She sees that her list is in there, named according to its
    # first list item
    wait_for(lambda: browser.find_element_by_link_text("Reticulate splines"))
    browser.find_element_by_link_text("Reticulate splines").click()
    wait_for(lambda: browser.current_url, first_list_url)

    # She decides to start another list, just to see
    browser.get(home_url)
    add_list_item(browser, "Click cows")
    second_list_url = browser.current_url

    # Under "my lists", her new list appears
    browser.find_element_by_link_text("My lists").click()
    wait_for(lambda: browser.find_element_by_link_text("Click cows"))
    browser.find_element_by_link_text("Click cows").click()
    wait_for(lambda: browser.current_url, second_list_url)

    # She logs out.  The "My lists" option disappears
    browser.find_element_by_link_text("Log out").click()
    wait_for(lambda: browser.find_elements_by_link_text("My lists"), [])
