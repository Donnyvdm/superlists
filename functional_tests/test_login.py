import os
import poplib
import re
import time

from django.core import mail
import pytest
from selenium.webdriver.common.keys import Keys

from functional_tests.utils import (
    wait_for,
    wait_to_be_logged_in,
    wait_to_be_logged_out,
    is_staging,
)

SUBJECT = "Your login link for Superlists"


@pytest.mark.functional
def test_can_get_email_link_to_log_in(selenium, live_server, home_url):

    browser = selenium
    if is_staging(home_url):
        test_email = "ediththetester@yahoo.com"
    else:
        test_email = "edith@example.com"

    # Edith goes to the awesome superlists site
    # and notices a "Log in" section in the navbar for the first time
    # It's telling her to enter her email address, so she does
    browser.get(home_url)
    browser.find_element_by_name("email").send_keys(test_email)
    browser.find_element_by_name("email").send_keys(Keys.ENTER)

    # A message appears telling her an email has been sent
    def check_email(browser):
        return "Check your email" in browser.find_element_by_tag_name("body").text

    wait_for(lambda: check_email(browser))

    # She checks her email and finds a message
    body = wait_for_email(home_url, test_email, SUBJECT)

    # It has a url link in it
    assert "Use this link to log in" in body
    url_search = re.search(r"http://.+/.+$", body)
    assert bool(url_search), f"Could not find url in email body: {body}"
    email_url = url_search.group(0)
    assert home_url[7:] in email_url

    # she clicks it
    browser.get(email_url)

    # she is logged in!
    wait_to_be_logged_in(browser, test_email)

    # Now she logs out
    browser.find_element_by_link_text("Log out").click()

    # She is logged out
    wait_to_be_logged_out(browser, test_email)


def wait_for_email(url, test_email, subject):
    if not is_staging(url):
        email = mail.outbox[0]
        assert test_email in email.to
        assert email.subject == subject
        return email.body

    email_id = None
    start = time.time()
    inbox = poplib.POP3_SSL("pop.mail.yahoo.com")
    try:
        inbox.user(test_email)
        inbox.pass_(os.environ["YAHOO_PASSWORD"])
        while time.time() - start < 60:
            # get 10 newest messages
            count, _ = inbox.stat()
            for i in reversed(range(max(1, count - 10), count + 1)):
                print("getting msg", i)
                _, lines, __ = inbox.retr(i)
                lines = [l.decode("utf8") for l in lines]
                print(lines)
                if f"Subject: {subject}" in lines:
                    email_id = i
                    body = "\n".join(lines)
                    return body
            time.sleep(5)
    finally:
        if email_id:
            inbox.dele(email_id)
        inbox.quit()
