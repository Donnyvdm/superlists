import time
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.keys import Keys

MAX_WAIT = 5


def is_local(url):
    return "localhost" in url or "127.0.0.1" in url


def is_staging(url):
    return "superlists-staging.donnyvdm.com" in url


def wait(fn):
    def modified_fn(*args, **kwargs):
        start_time = time.time()
        while True:
            try:
                return fn(*args, **kwargs)
            except (AssertionError, WebDriverException) as e:
                if time.time() - start_time > MAX_WAIT:
                    raise e
                time.sleep(0.5)

    return modified_fn


@wait
def wait_for_row_in_list_table(browser, row_text):
    table = browser.find_element_by_id("id_list_table")
    rows = table.find_elements_by_tag_name("tr")
    assert row_text in [row.text for row in rows]


@wait
def wait_for(f1, s2=None):
    if s2 is not None:
        assert f1() == s2
    else:
        assert f1()
    return


@wait
def wait_to_be_logged_in(browser, email):
    browser.find_element_by_link_text("Log out")
    navbar = browser.find_element_by_css_selector(".navbar")
    assert email in navbar.text


@wait
def wait_to_be_logged_out(browser, email):
    browser.find_element_by_name("email")
    navbar = browser.find_element_by_css_selector(".navbar")
    assert email not in navbar.text


def get_item_input_box(browser):
    return browser.find_element_by_id("id_text")


def get_error_element(browser):
    return browser.find_element_by_css_selector(".has-error")


def add_list_item(browser, item_text):
    num_rows = len(browser.find_elements_by_css_selector("#id_list_table tr"))
    get_item_input_box(browser).send_keys(item_text)
    get_item_input_box(browser).send_keys(Keys.ENTER)
    item_number = num_rows + 1
    wait_for_row_in_list_table(browser, f"{item_number}: {item_text}")
