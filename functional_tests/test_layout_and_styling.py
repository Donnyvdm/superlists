import pytest

from functional_tests.utils import get_item_input_box, add_list_item


@pytest.mark.functional
@pytest.mark.django_db
def test_layout_and_styling(selenium, live_server, home_url):

    browser = selenium
    browser.set_window_size(1024, 768)

    # Edith goes to the home page
    browser.get(home_url)

    # She notices the input box is nicely centered
    inputbox = get_item_input_box(browser)

    assert 512 == pytest.approx(
        inputbox.location["x"] + inputbox.size["width"] / 2, abs=10
    )

    # She starts a new list and sees the input is nicely
    # centered there too
    add_list_item(browser, "testing")

    inputbox = get_item_input_box(browser)
    assert 512 == pytest.approx(
        inputbox.location["x"] + inputbox.size["width"] / 2, abs=10
    )
