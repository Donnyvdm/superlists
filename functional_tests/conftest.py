import os
import socket
import pytest

from django.conf import settings

from .management.commands.create_session import create_pre_authenticated_session
from .server_tools import create_session_on_server, reset_database
from .utils import is_staging


@pytest.fixture(autouse=True)
def reset_staging(home_url):
    yield
    if is_staging(home_url):
        reset_database(staging_server)


@pytest.fixture(scope="function")
def selenium_factory(driver_class, driver_kwargs):
    driver_list = []

    def make_driver():
        driver = driver_class(**driver_kwargs)
        driver_list.append(driver)
        return driver

    yield make_driver

    for d in driver_list:
        d.quit()


@pytest.fixture(scope="function")
def authenticated_selenium(selenium, home_url):
    def generate_authenticated_browser(email):
        if is_staging(home_url):
            session_key = create_session_on_server(home_url, email)
        else:
            session_key = create_pre_authenticated_session(email)
        # to set a cookie we need to first visit the domain.
        # 404 pages load the quickest!
        selenium.get(home_url + "/404_no_such_url/")
        selenium.add_cookie(
            dict(name=settings.SESSION_COOKIE_NAME, value=session_key, path="/")
        )
        return selenium

    return generate_authenticated_browser


@pytest.fixture(scope="session")
def staging_server():
    return os.environ.get("STAGING_SERVER")


@pytest.fixture(scope="session")
def home_url(live_server):
    url = os.environ.get("STAGING_SERVER") or live_server.url
    if "0.0.0.0" in url:
        url = "http://{}:8000".format(socket.gethostbyname(socket.gethostname()))
    if not url.startswith("http"):
        url = "http://" + url
    return url


@pytest.fixture
def firefox_options(firefox_options, home_url):
    local_path = os.path.expanduser(
        "~/Applications/Firefox.app/Contents/MacOS/firefox-bin"
    )

    if os.path.exists(local_path):
        firefox_options.binary = local_path
    return firefox_options
